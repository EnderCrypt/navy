extends Node2D
class_name AbstractWeapon

@export var rotation_speed = TAU / 1;
@export var vision_range = 1000;
@export var projectile_scene: PackedScene;

var target: AbstractShip;


func _ready() -> void:
	assert(projectile_scene != null, "Missing projectile on "+get_name());
	rotation = randf() * TAU;


func get_ship() -> AbstractShip:
	return get_parent().get_parent() as AbstractShip;


func get_random_hostile() -> AbstractShip:
	var ships: Array = Rooms.fetch_level().get_ships();
	if ships.size() == 0:
		return null;
	var index: int = randi_range(0, ships.size() - 1);
	var ship: AbstractShip = ships[index];
	if ship.player == get_ship().player:
		return null;
	return ship as AbstractShip;


func get_aimed_object() -> Node2D:
	var from: Vector2 = global_position;
	var to: Vector2 = from + (Vector2.from_angle(global_rotation) * vision_range);
	var query: PhysicsRayQueryParameters2D = PhysicsRayQueryParameters2D.create(from, to);
	query.collision_mask = CollisionLayer.SHIP;
	var ray: Dictionary = get_world_2d().direct_space_state.intersect_ray(query);
	if ray.has("collider") == false:
		return null;
	return ray.collider as Node2D;


func is_aiming_at_hostile_ship() -> bool:
	var node: Node2D = get_aimed_object();
	if node == null:
		return false;
	if node is AbstractShip == false:
		return false;
	if get_ship().is_hostile(node) == false:
		return false;
	return true;


func _process(delta: float) -> void:
	if target == null and randf() < delta:
		target = get_random_hostile();
	if target != null:
		var distance: float = global_position.distance_to(target.global_position);
		if distance > vision_range:
			target = null;
	if target != null:
		global_rotation = global_position.direction_to(target.global_position).angle();
		if is_aiming_at_hostile_ship():
			shoot(); 


func shoot() -> void:
	var projectile: AbstractProjectile = projectile_scene.instantiate();
	Rooms.fetch_level().get_node("entities").add_child(projectile);
	projectile.global_position = $barrel.global_position;
	projectile.global_rotation = $barrel.global_rotation;
	projectile.velocity = Vector2.from_angle($barrel.global_rotation) * 10;
