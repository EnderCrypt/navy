extends CharacterBody2D
class_name AbstractProjectile


func _process(_delta: float) -> void:
	move_and_collide(velocity)
