extends AbstractOverlay

var start_vector: Vector2;
var active: bool = false;


func _process(delta: float) -> void:
	super._process(delta);
	if Input.is_action_just_pressed("drag_select"):
		start_vector = get_game_mouse_position();
		active = true;
	if Input.is_action_just_released("drag_select"):
		active = false;
		var end_vector: Vector2 = get_game_mouse_position();
		var x1: float = min(start_vector.x, end_vector.x);
		var y1: float = min(start_vector.y, end_vector.y);
		var x2: float = max(start_vector.x, end_vector.x);
		var y2: float = max(start_vector.y, end_vector.y);
		var width: float = x2 - x1;
		var height: float = y2 - y1;
		var area: Rect2 = Rect2(x1, y1, width, height);
		on_select_area(area);


func on_select_area(area: Rect2) -> void:
	for ship in Rooms.fetch_level().get_ships():
		if ship.player:
			var highlight: HighlightModule = ship.get_module("highlight");
			highlight.active = false;
			if area.has_point(ship.global_position):
				highlight.active = true;
			if ship.global_position.distance_to(area.position) < highlight.radius:
				highlight.active = true;
			if ship.global_position.distance_to(area.end) < highlight.radius:
				highlight.active = true;


func _draw() -> void:
	if active:
		var start: Vector2 = vector_to_screen(start_vector);
		var game_mouse: Vector2 = get_game_mouse_position();
		var end: Vector2 = vector_to_screen(game_mouse);
		var rect: Rect2 = Rect2(start, end - start);
		draw_rect(rect, Color.BLACK, false, 1.0);
