extends AbstractOverlay


func _draw() -> void:
	for ship in Rooms.fetch_level().get_ships():
		var highlight: HighlightModule = ship.get_module("highlight");
		if highlight.active:
			var objective: ObjectiveModule = ship.get_module("objective");
			if objective.has_destination():
				var direction: float = ship.global_position.direction_to(objective.get_destination()).angle();
				var start: Vector2 = ship.global_position + (Vector2.from_angle(direction) * highlight.radius);
				var end = objective.get_destination();
				draw_line(vector_to_screen(start), vector_to_screen(end), highlight.color, 3);
