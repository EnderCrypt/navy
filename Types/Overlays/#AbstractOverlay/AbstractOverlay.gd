extends Control
class_name AbstractOverlay


func _process(_ddelta: float) -> void:
	queue_redraw();


func get_game_mouse_position() -> Vector2:
	return Rooms.fetch_world().get_global_mouse_position();
	
	
func vector_to_screen(vector: Vector2) -> Vector2:
	var resolution: Vector2 = DisplayServer.window_get_size();
	var resolution_center: Vector2 = resolution / 2;
	var game: Game = Rooms.fetch_game();
	var camera: PlayerCamera = game.get_camera();
	var camera_position: Vector2 = camera.global_position;
	var camera_zoom: float = camera.camera_zoom;
	return (vector - camera_position) / camera_zoom + resolution_center;
