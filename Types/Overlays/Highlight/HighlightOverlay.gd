extends AbstractOverlay


func _process(_delta: float) -> void:
	queue_redraw();


func _draw() -> void:
	var camera: PlayerCamera = Rooms.fetch_game().get_camera();
	for ship in Rooms.fetch_level().get_ships():
		var highlight: HighlightModule = ship.get_module("highlight");
		if highlight.active:
			draw_circle(vector_to_screen(ship.global_position), highlight.radius / camera.camera_zoom, highlight.color);
