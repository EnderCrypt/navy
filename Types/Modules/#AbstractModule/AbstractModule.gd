extends Node
class_name AbstractModule


func get_ship() -> AbstractShip:
	return get_parent().get_parent() as AbstractShip;


func get_module(name: String) -> AbstractModule:
	return get_ship().get_module(name) as AbstractModule;


func get_global_mouse_position() -> Vector2:
	return Rooms.fetch_world().get_global_mouse_position();
