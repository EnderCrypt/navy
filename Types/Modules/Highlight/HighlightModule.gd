extends AbstractModule
class_name HighlightModule

@export var color: Color = Color(1, 0, 0, 0.1);
@export var radius: float = 100;

var active: bool = false;
