extends AbstractModule
class_name AiModule

func _process(_delta: float) -> void:
	var objective: ObjectiveModule = get_module("objective");
	var ship: AbstractShip = get_ship();
	
	if objective.has_destination():
		
		# thrust
		var distance_left: float = ship.global_position.distance_to(objective.get_destination());
		var distance_estimate: float = (ship.velocity * 20).length();
		ship.thrust = (distance_left - distance_estimate) / 50;
		ship.thrust = clamp(ship.thrust, 0, 1);
		
		# turn
		var angle_estimate: float = Misc.angle_difference(ship.rotation + ship.rotation_velocity, ship.global_position.angle_to_point(objective.get_destination()));
		ship.rudder = sign(angle_estimate);
		ship.rudder = clamp(ship.rudder, -1, 1);
