extends AbstractModule
class_name PhysicsModule

@export var acceleration: Curve;
var acceleration_bias: float = 0.5;

@export var turning: Curve;
var turning_bias: float = 0.00025;

@export var turning_friction: Curve;
var turning_friction_bias: float = 0.01;

@export var friction_relative: Curve;
var friction_relative_bias: float = 0.005;

@export var friction_absolute: Curve;
var friction_absolute_bias: float = 0.01;

@export var directional_transfer: Curve;
var directional_transfer_bias: float = 0.0025;

@export var weight_min: float = 1;
@export var weight_max: float = 1000;


func _process(_delta: float) -> void:
	var ship: AbstractShip = get_ship();
	assert(ship.thrust >= 0 && ship.thrust <= 1, "thrust cannot be "+str(ship.thrust));
	assert(ship.rudder >= -1 && ship.rudder <= 1, "rudder cannot be "+str(ship.rudder));
	# thrust gain
	ship.velocity += Vector2.from_angle(ship.rotation) * fetch(acceleration, acceleration_bias) * ship.thrust;
	# thrust loss (relative friction)
	ship.velocity *= 1 - fetch(friction_relative, friction_relative_bias);
	# thrust loss (absolute friction)
	ship.velocity = ship.velocity.normalized() * (ship.velocity.length() - fetch(friction_absolute, friction_absolute_bias));
	# rotation velocity
	ship.rotation_velocity += ship.rudder * fetch(turning, turning_bias) * ship.velocity.length();
	ship.rotation_velocity *= 1 - fetch(turning_friction, turning_friction_bias);
	ship.rotation_degrees += ship.rotation_velocity;
	# directional transfer
	redirect_velocity(fetch(directional_transfer, directional_transfer_bias), ship.rotation);
	pass;


func redirect_velocity(percent: float, angle: float) -> void:
	var ship: AbstractShip = get_ship();
	var velocity_transfer: Vector2 = ship.velocity * percent;
	ship.velocity -= velocity_transfer;
	ship.velocity += Vector2.from_angle(angle) * velocity_transfer.length();


func fetch(curve: Curve, bias: float) -> float:
	var weight: float = get_ship().weight;
	assert(weight >= weight_min, "weight ("+str(weight)+") cannot be less than "+str(weight_min));
	assert(weight <= weight_max, "weight ("+str(weight)+") cannot be more than "+str(weight_max));
	var x: float = (weight - weight_min) / (weight_max - weight_min);
	var y: float = curve.sample(x);
	# print("x: "+str(x)+" y: "+str(y));
	return y * bias;
