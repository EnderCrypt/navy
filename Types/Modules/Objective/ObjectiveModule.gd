extends AbstractModule
class_name ObjectiveModule

var target_travelling_towards: bool; # ensures target is disabled when travelling past it
var previous_distance_left: float;
var destination: Node2D;


func _ready():
	reset();


func set_destination(vector: Vector2) -> void:
	reset();
	destination = Node2D.new();
	destination.global_position = vector;
	Rooms.fetch_level().add_child(destination);


func get_destination() -> Vector2:
	assert(has_destination(), "missing destination");
	return destination.global_position;


func has_destination() -> bool: 
	return destination != null;


func reset() -> void:
	target_travelling_towards = false;
	previous_distance_left = 0;
	if destination != null:
		destination.queue_free();
	destination = null;


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_mask == MouseButton.MOUSE_BUTTON_RIGHT:
		if get_module("highlight").active:
			set_destination(get_global_mouse_position());


func _process(_delta: float) -> void:
	var ship: AbstractShip = get_ship();
	if has_destination():
		# values
		var distance_left: float = ship.global_position.distance_to(get_destination());
		var travelling_towards: bool = previous_distance_left > distance_left;
		previous_distance_left = distance_left;
		# check
		if travelling_towards and target_travelling_towards == false:
			target_travelling_towards = true;
		if travelling_towards == false and target_travelling_towards:
			reset();
