extends CharacterBody2D
class_name AbstractShip

@export var player: bool = false;
@export var weight: float = 1;

var rotation_velocity: float = 0;
var thrust = 0; # 0 to 1
var rudder = 0; # -1 to 1


func get_module(name: String) -> AbstractModule:
	return get_node("./module/"+name) as AbstractModule;


func is_friendly(other: AbstractShip) -> bool:
	return player == other.player;


func is_hostile(other: AbstractShip) -> bool:
	return player != other.player;


func _process(delta: float) -> void:
	var collision: KinematicCollision2D = move_and_collide(velocity * delta);
	if collision != null:
		var other: Node2D = collision.get_collider();
		print(get_name()+" collided with "+other.get_name());
