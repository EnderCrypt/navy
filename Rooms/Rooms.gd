extends Node


func fetch(path: String) -> Node:
	var node: Node = get_tree().root.get_node("/root/"+path);
	assert(node != null, "Missin node: "+path);
	return node;


func fetch_main() -> Main:
	return fetch("Main");


func fetch_game() -> Game:
	return fetch("Main/Game");


func fetch_level() -> AbstractLevel:
	return fetch_game().level;


func fetch_world() -> Node2D:
	return fetch("Main/Game/World");
