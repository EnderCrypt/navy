extends Node
class_name Main

var scene: Node = null;

func _ready() -> void:
	start_level("Iceland");
	pass;

func start_level(country: String) -> void:
	var resource: String = "res://Maps/"+country+"/"+country+".tscn";
	assert(ResourceLoader.exists(resource), "Missing level for country: "+country+" ("+resource+")");
	var game: Game = load("res://Rooms/Game/Game.tscn").instantiate();
	game.level = load(resource).instantiate();
	print("Starting country level: "+country);
	set_scene(game);

func set_scene(scene: Node) -> void:
	if self.scene != null:
		self.scene.queue_free();
	self.scene = scene;
	print("Switched scene to "+self.scene.scene_file_path);
	add_child(self.scene);

func get_scene() -> Node:
	assert(self.scene != null, "No scene currently set");
	return self.scene;
