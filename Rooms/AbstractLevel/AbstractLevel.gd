extends Node2D
class_name AbstractLevel

const tile_out_of_map = -1;
const tile_water = 2;
const tile_land = 3;


func get_tile(location: Vector2) -> int:
	var hexagon: Vector2 = $land.local_to_map(location);
	return $land.get_cell_source_id(0, hexagon, true);


func is_out_of_map(location: Vector2) -> bool:
	return get_tile(location) == tile_out_of_map;


func is_land(location: Vector2) -> bool:
	return get_tile(location) == tile_land;


func is_water(location: Vector2) -> bool:
	return get_tile(location) == tile_water;


func get_ships():
	var nodes: Array[Node] = $entities.get_children();
	var matching = nodes.filter(func(node): return node is AbstractShip);
	return matching as Array[AbstractShip];
