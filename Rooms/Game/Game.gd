extends Node
class_name Game

@export var level: AbstractLevel;


func _ready():
	$World.add_child(level);


func get_level() -> AbstractLevel:
	return level;


func get_camera() -> PlayerCamera:
	return get_node("./World/Camera/");
