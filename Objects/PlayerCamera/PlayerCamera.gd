extends Camera2D
class_name PlayerCamera

@export var motion_multiplier: float = 10.0;
@export var motion_friction: float = 0.2;
@export var zoom_multiplier: float = 0.05;
@export var zoom_friction: float = 0.25;
@export var zoom_minimum: float = 0.5;
@export var zoom_maximum: float = 25;
@export var out_of_map_gravity: float = 0.0025;

var camera_motion: Vector2 = Vector2.ZERO;
var camera_zoom: float = 20.0;
var camera_zoom_motion: float = 0.0;

func _input(event: InputEvent) -> void:
	var zoom_value: float = event.get_action_strength("camera_zoom_out") - event.get_action_strength("camera_zoom_in");
	camera_zoom_motion += zoom_value * camera_zoom * zoom_multiplier;


func _process(_delta: float) -> void:
	# motion
	camera_motion += get_input_vector() * camera_zoom * motion_multiplier;
	global_position += camera_motion;
	camera_motion *= (1.0 - motion_friction);
	# zoom
	camera_zoom += camera_zoom_motion;
	camera_zoom = clamp(camera_zoom, zoom_minimum, zoom_maximum);
	camera_zoom_motion *= (1.0 - zoom_friction);
	zoom.x = 1 / camera_zoom;
	zoom.y = 1 / camera_zoom;
	# out of map
	if Rooms.fetch_level().is_out_of_map(global_position):
		camera_motion -= global_position * out_of_map_gravity;


func get_input_vector() -> Vector2:
	var input_vector: Vector2 = Vector2.ZERO;
	input_vector.x += Input.get_action_strength("camera_right") - Input.get_action_strength("camera_left");
	input_vector.y += Input.get_action_strength("camera_down") - Input.get_action_strength("camera_up");
	return input_vector;
