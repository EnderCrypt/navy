extends Node


func angle_difference(from: float, to: float) -> float:
	return Vector2.from_angle(from).angle_to(Vector2.from_angle(to));
